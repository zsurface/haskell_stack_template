module AronSimple where

{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE UnicodeSyntax #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE DuplicateRecordFields #-}

import Control.Monad
import Control.Monad.IO.Class (liftIO)
import Control.Concurrent
import Data.Array.IO
import Data.Char 
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Ratio
import Data.Maybe (fromJust, isJust, fromMaybe)
import Data.Time.Clock.POSIX
import Data.Foldable (foldrM)
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import Data.Typeable 
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Posix.Types
import System.Process
import System.Random
import Text.Read (Read)
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Text.Printf
import Debug.Trace (trace)
import Text.Pretty.Simple (pPrint)

import qualified Data.Vector         as V
import qualified Data.HashMap.Strict as M
import qualified Text.Regex.TDFA     as TD
import qualified Text.Regex          as TR
import qualified Data.Set            as DS
import qualified Data.Word           as DW 

import qualified Data.ByteString.Lazy      as BL
import qualified Data.ByteString.Lazy.Char8 as LC8 
import qualified Data.ByteString.Lazy.UTF8 as BLU
import qualified Data.ByteString.Lazy.Internal as IN (ByteString)
import qualified Data.ByteString.UTF8      as BSU
import qualified Data.ByteString           as BS
import qualified Data.Text.Lazy            as TL
import qualified Data.Text                 as TS
import qualified Data.Text.IO              as TSO
import qualified Data.Text.Encoding        as TSE
import qualified Data.Text.Lazy.Encoding   as TLE
import qualified Data.ByteString.Char8     as S8 (putStrLn, putStr, lines)   -- strict ?
import qualified Data.ByteString.Internal  as BSI (c2w) 
import qualified Data.Array as DR 
import Data.Array.IO

-- import Rainbow
-- import System.Console.Pretty (Color (..), Style (..), bgColor, color, style, supportsPretty)

{-|
  === any dimension list
  * 2*[1, 2] = [2, 4]
  * [1, 2]+[3, 4] = [4, 6]
  * 2*[[1, 2]] = [[2, 4]]
-}
instance Num a => Num [a] where 
    (+) = zipWith (+) 
    (-) = zipWith (-) 
    (*) = zipWith (*)  
    abs = map abs 
    signum = map signum 
    fromInteger = repeat . fromInteger




{-| 
    >(round . (* 10^12)) <$> getPOSIXTime 
-} 
timeNowPico::IO Integer
timeNowPico = (round . (* 10^12)) <$> getPOSIXTime 

{-| 
    >(round . (* 10^9)) <$> getPOSIXTime 
-} 
timeNowNano::IO Integer
timeNowNano = (round . (* 10^9)) <$> getPOSIXTime 

{-| 
    >(round . (* 10^6)) <$> getPOSIXTime 
-} 
timeNowMicro::IO Integer
timeNowMicro = (round . (* 10^6)) <$> getPOSIXTime 

{-| 
    >(round . (* 10^3)) <$> getPOSIXTime 
-} 
timeNowMilli::IO Integer
timeNowMilli = (round . (* 10^3)) <$> getPOSIXTime 

{-| 
    >(round . (* 1)) <$> getPOSIXTime 
-} 
timeNowSecond::IO Integer
timeNowSecond = (round . (* 1)) <$> getPOSIXTime 

{-| 
    === Trim, remove whitespace characters from either side of string.

    see 'trimWS' all whitespace
-} 
trim::String->String -- trim ws from both sides
trim s  = TS.unpack $ TS.strip $ TS.pack s

{-| 
    === KEY: split string, split str

    > splitStr "::" "dog::cat" => ["dog", "cat"]
-} 
splitStr::String -> String -> [String]  -- splitStr "::" "dog::cat" => ["dog", "cat"]
splitStr r s = splitRegex (mkRegex r) s

{-| 
    === Partition string to [String] according to character class []

    @
    splitStrChar "[,.]" "dog,cat,cow.fox" => ["dog", "cat", "cow", "fox"]y
    splitStrChar::String->String->[String]
    splitStrChar r s = splitWhen(\x -> matchTest rex (x:[])) s
                where
                    rex = mkRegex r
    @

    * See 'splitStrCharTrim'

    >splitStrRegex => splitStrChar
-} 
splitStrChar::String->String->[String]  -- splitStrChar "[,.]" "dog,cat,cow.fox" => ["dog", "cat", "cow", "fox"]
splitStrChar r s = splitWhen(\x -> matchTest rex (x:[])) s
                where
                    rex = mkRegex r

{-|
    === Split String. 'trim' and Remove empty String
    @
    splitStrCharTrim "[,.]" " dog,fox. " => ["dog", "fox"]
    @

    * See 'splitStrChar'
-}
splitStrCharTrim::String -> String ->[String]
splitStrCharTrim r s = filter (\x -> len x > 0) $ map trim $ splitWhen(\x -> matchTest rex (x:[])) s
                where
                  rex = mkRegex r


{-| 
    === Match all pat from a given str
-} 
matchAllBS::BS.ByteString -> BS.ByteString -> [(MatchOffset, MatchLength)]
matchAllBS pat str = join $ fmap DR.elems $ matchAll (makeRegex pat :: Regex) str

{-|
    === Better length function

    * Convert Int to polymorphic values
    * Convert Int to Num
    * fromIntegral::(Integral a, Num b)=> a -> b
 -}
len::(Foldable t, Num b)=>t a -> b
len a = fromIntegral $ length a

{-| 
    === split key and value

    >splitStrTuple "="  "host = localhost" => (host, localhost)
    * TODO: fix the function if  host = dog = cat => ("host", "dogcat")
-} 
splitStrTuple::String -> String -> (String, String)
splitStrTuple p s = (trim $ head $ splitStr p s, trim $ last $ splitStr p s)

